var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const CategorySchema = new Schema({

    title: {
        type: String,
        required: true
    },
    creationDate: {
        type: Date,
        default: Date.now()
    },

});

module.exports = mongoose.model('Category', CategorySchema );

