var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcryptjs');
const UserSchema = new Schema({

    firstname: {
        type: String,
        // required: true
    },
    lastname: {
        type: String,
        // required: true
    },
    email: {
        type: String,
        // required: true
    },
    password: {
        type: String,
        // required: true
    },

});

// checks if password is valid
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var User = module.exports = mongoose.model('User', UserSchema );

module.exports.getUserByEmail = function (email, callback) {
    var query = {email :email};
    User.findOne(query, callback);
}
module.exports.getUserById = function (id, callback) {
    var data = User.findById(id, callback);
}

module.exports.compareUserByPassword = function (postPassword, hash, callback) {
    bcrypt.compare(postPassword, hash, function(err, isMatch){
        if(err) throw  err;
        callback(null, isMatch);
    });
}
