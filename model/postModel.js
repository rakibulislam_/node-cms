var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const PostSchema = new Schema({

    title: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: 'public'
    },
    description: {
        type: String,
        required: true
    },
    creationDate: {
        type: Date,
        default: Date.now()
    },
    allowComments: {
        type: Boolean,
        default: false
    },
   user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
     category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    comments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    file: {
        type: String,
        default: ''
    }
});

module.exports = mongoose.model('Post', PostSchema );

