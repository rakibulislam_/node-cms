module.exports = {
    // connectionUrl : 'mongodb+srv://rakibul:rakibul123@cms-tutorial.2bpj7.mongodb.net/cms-tutorial?retryWrites=true&w=majority',
    connectionUrl : 'mongodb+srv://rakibul:rakibul123@cms-tutorial.2bpj7.mongodb.net/cms-tutorial?retryWrites=true&w=majority',
    // connectionUrl : 'mongodb://localhost:27017/cms_tutorial, {useNewUrlParser: true, useUnifiedTopology: true}',

    // works as middleware + go through all over the project
    globalVariables : (req, res, next)=>{
        res.locals.success_message = req.flash('success_message');
        res.locals.error_message = req.flash('error_message');
        res.locals.user = req.user || null;
        console.log("Session User - "+res.locals.user)
        next(); /*need to use other wise loop will stuck*/
    },

};