// Importing modules
const express = require('express');
const mongoose = require('mongoose');
var path = require('path');
const app = express();
var exphbs = require('express-handlebars');
const {connectionUrl} = require('./config/configuration');
var bodyParser = require("body-parser");
const {globalVariables} = require("./config/configuration");
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access');
const Handlebars = require('handlebars');
// for file upload
const fileUpload = require('express-fileupload');
// passport
const passport = require('passport');

// for global variable package
const flash = require('connect-flash');
const session = require('express-session');


// DB Connection Created
mongoose.connect(connectionUrl, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.once('open', function() {
    console.log("DB Connection Created");
}).on('error', function(error) {
    console.log("Db Error - " + error);
})

// body Parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/*file Upload middleware*/
app.use(fileUpload());
// configure view engine
const hbs = exphbs.create({
    defaultLayout: 'default',
    handlebars: allowInsecurePrototypeAccess(Handlebars),
});
app.engine('handlebars', hbs.engine);
// app.engine('handlebars', exphbs({defaultLayout: 'default', }));
app.set('view engine', 'handlebars');

// configure express
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

// flash & session
app.use(session({
    secret: 'anysecret',
    resave: true,
    saveUninitialized: true,
    // cookie: { secure: true }
}));
app.use(flash());
app.use(globalVariables);

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Routes
const defaultRoutes = require('./routes/defaultRoutes');
const adminRoutes = require('./routes/adminRoutes');
app.use('/', defaultRoutes);
app.use('/admin', adminRoutes);

// port
const port = '3000'
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})
