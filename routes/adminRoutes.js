const express = require('express');
const router = express.Router()
const adminController = require('../controller/adminController');

const {isUserAuthenticated} = require('../config/customConfigure');

// isUserAuthenticated will be used to make stop access of assessed user form front end to admin

router.all('/*', isUserAuthenticated,(req, res, next) => {
    req.app.locals.layout = 'admin';
    next();
})

/*route for Post*/
router.route('/').get(adminController.index);
router.route('/posts')
    .get(adminController.getPosts)
    .post(adminController.submitPosts);

router.route('/create-posts')
    .get(adminController.createPosts);

router.route('/posts/edit/:id')
    .get(adminController.editPost);

router.route('/posts/update')
    .post(adminController.updatePosts)

router.route('/posts/delete/:id')
    .post(adminController.deletePost);


/*route for Category*/
router.route('/category')
    .get(adminController.getCategory)
    .post(adminController.submitCategory);

router.route('/create-category')
    .get(adminController.createCategory);

router.route('/category/edit/:id')
    .get(adminController.editCategory);

router.route('/category/update')
    .post(adminController.updateCategory);

router.route('/category/delete/:id')
    .post(adminController.deleteCategory);

router.route('/getComments')
    .get(adminController.getComment)

module.exports = router;