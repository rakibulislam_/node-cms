const express = require('express');
const router = express.Router()
const defaultController = require('../controller/defaultController');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../model/userModel');

router.all('/*', (req, res, next) => {
    req.app.locals.layout = 'default';
    next();
})

router.route('/').get(defaultController.index);


passport.use(new LocalStrategy({
    usernameField : 'email',
    },
    function(email, password, done) {
        User.getUserByEmail(email, (err, user)=>{
            if(err) throw  err;
            if(!user){
                return done(null, false, { error_message: 'Incorrect Email !' });
            }
            User.compareUserByPassword(password, user.password, (err, isMatch)=>{
                if(err) throw  err;
                if(!isMatch){
                    return done(null, false, { error_message: 'Incorrect Password !' });
                }
                else{
                    console.log("Logged User - "+user);
                    return done(null, user);
                }
            })
        })

    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
    console.log("Serialize Data Id - "+user.id);
});

passport.deserializeUser(function(id, done) {
    User.getUserById(id, function(err, user) {
        done(err, user);
        console.log("Deserialize Data - "+user);
    });
});

router.route('/login')
    .get(defaultController.loginGet)
    .post(passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true}),defaultController.loginPost);

router.route('/register')
    .get(defaultController.registerGet)
    .post(defaultController.registerPost);

router.route('/logout')
    .get(defaultController.logout)

router.route('/post-details/:id')
    .get(defaultController.getPostByID)
    .post(defaultController.submitComment)

/*router.route('/post-by-category-id/:id')
    .get(defaultController.getPostByCategoryID)*/

/*router.route('/comments')
    .get(defaultController.submitComment)*/

module.exports = router;