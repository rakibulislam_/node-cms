const Post = require('../model/postModel');
const Category = require('../model/categoryModel');
const Comment = require('../model/commentModel');
const {connectionUrl} = require('../config/configuration');
const {isEmpty} = require('../config/customConfigure');

module.exports = {

    /*action for Posts*/
    index: (req, res) => {
        res.render('admin/index');
    },

    getPosts:(req,res)=>{
        Post.find()
            .populate('category')
            .then(posts => {
                console.log('All Post :' + posts);
                res.render('admin/posts/index', {posts: posts});
            });
    },

    submitPosts:(req, res)=>{
        // for checkbox. cause ceckbox gives on/off
        const allowedComments = req.body.allowComments ? true : false ;


        // Check for any input file
        let filename = '';
        if(!isEmpty(req.files)) {
            let file = req.files.uploadedFile;
            filename = req.body.title+file.name;
            let uploadDir = './public/uploads/';

            file.mv(uploadDir+filename, (err) => {
                if (err)
                    throw err;
            });
        }

        let _newPost = new Post();
            _newPost.title = req.body.title;
            _newPost.description = req.body.description;
            _newPost.status = req.body.status;
            _newPost.allowComments = allowedComments;
            _newPost.category = req.body.category;
            _newPost.category = req.body.category;
            _newPost.file = `/uploads/${filename}`

        _newPost.save().then(post => {
            console.log('Submitted Post :' + post);
            req.flash('success_message','Data Saved Successfully');
            res.redirect('/admin/posts');
        });
    },

    createPosts:(req,res)=>{
        Category.find()
            .then(category => {
                console.log('All category :' + category);
                res.render('admin/posts/create', {category: category});
            });
    },


    editPost:(req,res)=>{
        const id = req.params.id;
        Post.findById(id)
            .populate('category')
            .then(editedPost =>{
                Category.find().then(cats => {
                    console.log('Edited cats :' + cats);
                    res.render('admin/posts/edit',{'editedPost':editedPost,'category':cats});
                })
            console.log('Edited Post :' + editedPost);
        });

    },

    updatePosts: async  (req, res) => {
        const allowedComments = req.body.allowComments ? true : false ;
        const id = req.body.id;
        const getPostData = await Post.findById(id);
        // Check for any input file
        let filename = '';
        if(!isEmpty(req.files)) {
            let file = req.files.uploadedFile;
            filename = '/uploads/'+req.body.title+file.name;
            let uploadDir = './public';

            file.mv(uploadDir+filename, (err) => {
                if (err)
                    throw err;
            });
        }
        else {
            filename = getPostData.file;
        }
        Post.findById(id)
            .then(post =>{
                post.title = req.body.title;
                post.description = req.body.description;
                post.status = req.body.status;
                post.allowComments = allowedComments;
                post.category = req.body.category;
                post.file = filename;

                post.save().then(updatedpost => {
                    console.log('Updated Post :' + updatedpost);
                    req.flash('success_message','Data Updated Successfully');
                    res.redirect('/admin/posts');
                });
            });
    },

    deletePost: (req, res) => {
        Post.findByIdAndRemove(req.params.id)
            .then(deletedPost => {
                req.flash('success_message', `The post ${deletedPost.title} has been deleted.`);
                res.redirect('/admin/posts');
            });
    },

    /*action for Category*/
    getCategory: (req, res) =>{
        Category.find()
            // .populate('category')
            .then(category => {
                console.log('All Category :' + category);
                res.render('admin/category/index', {category: category});
            });
        },

    createCategory: (req, res) =>{
        res.render('admin/category/create');
    },

    submitCategory: (req, res) =>{
        const title = req.body.title;
        let _newCategory = new Category();
        _newCategory.title = title;

        _newCategory.save().then(category => {
            console.log('Submitted Category :' + category);
            req.flash('success_message','Data Saved Successfully');
            res.status(200).json(category);
        });
    },
    editCategory: (req, res) =>{
        const id = req.params.id;
        Category.findById(id)
            .then(editedCategory =>{
                console.log('Edited cats :' + editedCategory);
                res.render('admin/category/edit',{'category':editedCategory});
            });
    },

    updateCategory: (req, res) => {
        const id = req.body.id;
        const title = req.body.title;
        console.log(id);
        Category.findById(id)
            .then(category =>{
                category.title = title;

                category.save().then(updatedCategory => {
                    console.log('Updated Category :' + updatedCategory);
                    req.flash('success_message','Data Updated Successfully');
                    res.status(200).json(category);
                });
            });
    },

    deleteCategory: (req, res) =>{
        const id = req.params.id;
        console.log(id);
        Category.findByIdAndRemove(id)
            .then(deletedCategory => {
                console.log('Deleted Category ' +deletedCategory);
                req.flash('success_message', `The post ${deletedCategory.title} has been deleted.`);
                res.redirect('/admin/category');
            });
    },

    /* COMMENT ROUTE SECTION*/
    getComment: (req, res) => {
        Comment.find()
            .populate('user')
            .then(comments => {
                res.render('admin/comments/index', {comments: comments});
            })
    }
}
