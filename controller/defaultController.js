const Post = require('../model/postModel');
const Comment = require('../model/commentModel');
const Category = require('../model/categoryModel');
const User = require('../model/userModel');
const bcrypt = require('bcryptjs');
module.exports = {

    index: async  (req, res) => {
        const post = await Post.find();
        const category = await Category.find();
        console.log("All Post" + post);
        res.render('default/index',{'posts':post,'category':category});
    },

    loginGet: (req, res) => {
        res.render('default/login');
    },

    loginPost: (req, res) => {
        res.redirect('/login');
    },

    logout : (req, res)=>{
        req.logout();
        req.flash('success_message','You have logged out successfully ...');
        res.redirect('/login');
    },

    registerGet: (req, res) => {
        res.render('default/register');
    },

    registerPost: (req, res) => {
        let error = [];

        if(!req.body.firstname){
            error.push({message: 'Firstname is Empty !'});
        }
        if(!req.body.lastname){
            error.push({message: 'Lastname is Empty !'});
        }
        if(!req.body.email){
            error.push({message: 'Email is Empty !'});
        }
        if(!req.body.password){
            error.push({message: 'Password is Empty !'});
        }
        if(req.body.password != req.body.confirmPassword){
            error.push({message: 'Password do not Match !'});
        }
        if(error.length > 0){
            console.log(error);
            res.render('default/register',{
                error: error,
                firstname : req.body.firstname,
                lastname : req.body.lastname,
                email : req.body.email,
            });
        }
        else {
            User.findOne({email: req.body.email}).then(user =>{
                if(user){
                    req.flash('error_message', `Email already exists! Try to Login... `);
                    res.redirect('register');
                }
                else{
                    console.log(req.body);
                    const newUser = new User(req.body);
                    bcrypt.genSalt(10, function(err, salt) {
                        bcrypt.hash(newUser.password, salt, function(err, hash) {
                            newUser.password = hash;
                            newUser.save().then(register => {
                                console.log("new Register "+ register);
                                res.redirect('/login');
                                req.flash('success_message', `User Register Successfully... `);
                            });
                        });
                    });
                }
            });
        }
    },

    getPostByID: async (req, res) =>{
        const id = req.params.id;
        const category = await Category.find();
        Post.findById(id)
            // .populate({path: 'getComments', populate:{path:'user', model:'user'}})
            .then(post =>{
            if(!post){
                res.status(400).json({message:"Post Not Found"});
            } else {
                console.log("Comment : "+post.comments);
                res.render('default/post_detail',{'post_details':post,'category':category,'comment':post.comments});
            }
        });
    },

    submitComment: (req, res)=>{
        console.log(req.body.comment_body);
        console.log(req.body.id);
        Post.findById(req.body.id).then(post =>{
            let _comment = new Comment();
            _comment.user = req.body.id;
            _comment.body = req.body.comment_body;

            post.comments.push(_comment);
            post.save().then(post=>{
                _comment.save().then(newComment=>{
                    req.flash('success_message',"Comment Submitted For Review");
                    res.redirect(`/post-details/${post._id}`);
                })
            })
        });
        if(req.user){
            Post.findById(req.body._id).then(post =>{
                let _comment = new Comment();
                _comment.user = req.user.id;
                _comment.body = req.body.comment_body;

                post.comments.push(_comment);
                post.save().then(post=>{
                    _comment.save().then(newComment=>{
                        req.flash('success_message',"Comment Submitted For Review");
                        res.redirect('/post-details/${post._id}');
                    })
                })
            });
        }
        else{
            req.flash('error_message',"Login first");
            res.redirect('/login');

        }
        /*const body = req.body.commentBody;
        let _comment = new Comment();
        _comment.body = body;
        _comment.save().then(comment =>{
            console.log("Created Comment - "+comment);
            req.flash('success_message','Wait for the comment to be accepted');
            res.render('default/index');
        });*/
    },

    /*getPostByCategoryID: (req, res)=>{
        const id = req.params.id;
        const data = db.collection('posts').find('category',ObjectId(id));
        console.log(data);
        /!*Post.findOne(ObjectId("568c28fffc4be30d44d0398e")).then(categoryPost=>{
            console.log(categoryPost);
        });*!/
    }*/
}

